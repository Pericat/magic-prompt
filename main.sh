#!/bin/bash
source quit.sh
source listing.sh
source deleteFile.sh
source deleteDirectory.sh
source about.sh
source version.sh
source ageNum.sh
source getHTML.sh
source time.sh
source unknownCommand.sh
source openVim.sh
source showDirectory.sh
source profil.sh
source loginInfo.sh
source passw.sh
source userDatabase.sh
source email.sh


cmd() {
  cmd=$1
  argv=$*
  outCmd=$(echo $argv | sed "s/${cmd}//")

  case "${cmd}" in
    quit | exit ) quit;;
    ls ) listing;;
    rm ) deleteFile "$outCmd";;
    rmd | rmdir ) deleteDirectory "$outCmd";;
    about ) aboutProject;;
    version | --v | vers ) projectVersion;;
    age ) age;;
    httpget) getHTML "$outCmd";;
    hour ) hourTime;;
    open ) openVimFile "$outCmd";;
    pwd ) showDir;;
    profil ) profilInfo;;
    passw ) changePassword;;
    smtp ) sendEmail;;
    * ) unknownC "$cmd";;
  esac
}

main() {
  lineCount=1
  userName=""

  login

  while [ 1 ]; do
    date=$(date +%H:%M)
    echo -ne "${date} - [\033[31m${lineCount}\033[m] - \033[33mXzen\033[m ~ ☠️ ~ "
    read string

    cmd $string
    lineCount=$(($lineCount+1))
  done
}

main
