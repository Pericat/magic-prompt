# magic-prompt

Ce projet est un reproduction du terminal bash, reprenant certaines commandes et ajoutant une nouvel


# Commandes

icic sont listé les commandes utilisables

## ls

list file and directory

## rm FileName
Delete named file


## rmd/rmdir fileName

Delete named directory

## about

Project descritpion

## version

actual magic prompt version is 1.0.0

## age
enter your age, prompt tell if minor or adult

## quit
quit the prompt

## profil
list user name, last name, age, email of current user

## passw
allow current user to change password

## cd
change directory

## pwd
show relative path of where you are

## hour
show time

## *
command unknown

## httpget url

user enter user and download html in a new file

## smtp
allow user to send email, you need to enter target email, email subject and body

## open
open file in vim en if the file doesn't exist